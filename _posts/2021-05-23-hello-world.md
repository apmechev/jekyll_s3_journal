---
layout: post
title:  "Hello"
author: apmechev
categories: [ journal]
image: assets/images/1.jpg
featured: true
hidden: false
comments: false
---

Hi. This is my journal.

I'm not really a fan of the word `blog`. As Yahtzee Croshaw [said](https://www.goodreads.com/quotes/615130-readers-of-my-online-journal---i-refuse-to-use)

> I refuse to use the word blog because it sounds like something that lives on a riverbed and communicates through farts


But they say that journaling is beneficial to one's ideation<sup><sup>[citation needed]</sup></sup>, so let's give it a try. Most of my life is spent in front of the computer, both professional and as a hobby. Thus, I should expect a lot of the content here to be computing related. I'll try to spice it up though. 

Small tidbit: The first reference to the 'Hello World' program comes in a tutorial for the `B` programming language by Brian Kernighan published in 1973. Allegedly the origin comes from a cartoon of a chick hatching from an egg and saying 'Hello World'. In a way B, and more-so `C` developed by Dennis Ritchie, were the beginning of a new era of computation. Since the early 70's, interfacing with computers has been done in a more rich and expressive way, allowing you to `print("Hello World")`.<sup><sup>[SOURCE](https://blog.hackerrank.com/the-history-of-hello-world/)</sup></sup>

Smell ya later, 

Alexandar

----------------
----------------

Header image by author.














