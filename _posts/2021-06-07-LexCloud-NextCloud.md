---
layout: post
title:  "Learnings from deploying NextCloud"
author: apmechev
categories: [ AWS,  self-hosted]
image: assets/images/2.jpg
featured: true
hidden: false
comments: false
---

I've been on a self-hosting journey for a while now. 

There are several services that I've ran at various levels of production-ness, but configuring NextCloud on an instance already running Bookstack was quite a bit of a challenge. 

The best way to learn is to do. So after I deployed [Bookstack](https://www.bookstackapp.com/) on my t2.micro, I decided to also deploy NextCloud on the same Instance. And of course, let's not use Docker, let's make things challenging. 

In theory the two services would work together using the same database, however the Bookstack installer runs on MySQL, while Nextcloud requires MariaDB. While MariaDB is forked off MySQL, and thus should be compatible, installing it broke the MySQL server. Luckily I was able to backup my bookstack database. Then I had to find out how to fix a [very broken package](https://askubuntu.com/questions/148715/how-to-fix-package-is-in-a-very-bad-inconsistent-state-error/510887#510887), I was able to install a clean MariaDB, faff about with permissions for my `root` and `bookstack` user, and finally restore the Bookstack database.

From then on, [Installing NextCloud](https://docs.nextcloud.com/server/latest/admin_manual/installation/source_installation.html) was quite straight-forward. Just have the right permissions on the `/var/www/nextcloud` folder; and it was good to go! Surprisingly, it works quite well on a t2.micro although I do plan on moving it to "on-prem" later in the year.

On to the next one!

Alexandar

----------------
----------------

Header image by author.














